FROM alpine:latest

MAINTAINER Aleksandr Alekseev

ENV mtrf_serial_port_env=/dev/tty.mtrf_serial_port \
    mqtt_scheme=mqtt \
    mqtt_host=127.0.0.1 \
    mqtt_port=__EMPTY__ \
    mqtt_user=__EMPTY__ \
    mqtt_password=__EMPTY__ \
    commands_delay=0.1 \
    logging_level=INFO

RUN apk --update add python3 && rm -rf /var/cache/apk/*

RUN pip3 install --upgrade noolite-mtrf-mqtt

CMD noolite_mtrf_mqtt \
        --mtrf-serial-port=$mtrf_serial_port_env \
        --mqtt-scheme=$mqtt_scheme \
        --mqtt-host=$mqtt_host \
        --mqtt-port=$mqtt_port \
        --mqtt-user=$mqtt_user \
        --mqtt-password=$mqtt_password \
        --commands-delay=$commands_delay \
        --logging-level=$logging_level
